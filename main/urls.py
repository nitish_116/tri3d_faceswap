from django.conf.urls import url
from . import views

app_name = 'main'
urlpatterns = [
	url(r'^$', views.index,name="index"),
	url(r'^upload_source_image/$', views.upload_source_image,name="upload_source_image"),
	url(r'^upload_dest_image/$', views.upload_dest_image,name="upload_dest_image"),
	url(r'^blend/$', views.blend,name="blend"),
]

from django import forms
from django.conf import settings

class ImageForm(forms.Form):
    image = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': False}))
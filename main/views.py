from django.shortcuts import render,redirect,get_object_or_404
from django.http import HttpResponse
import os
from .forms import ImageForm
from .models import Image
from django.core.files import File
from django.conf import settings


def index(request):
    source_image = None
    dest_image = None
    output_image = None
    for image in Image.objects.all():
        if image.im_type == 'source':
            source_image = image
        if image.im_type == 'dest':
            dest_image = image
        if image.im_type == 'output':
            output_image = image

    C = {
        'source_image' : source_image,
        'dest_image' : dest_image,
        'output_image' : output_image,
    }
    print(source_image.im_field.url)

    return render(request,'main/index.html',C)


def upload_source_image(request):
    
    form  = ImageForm(request.POST or None, request.FILES or None)
    
    if form.is_valid():
        for image in Image.objects.all():
            if image.im_type == 'source':
                image.delete()
        I = Image(im_type='source',im_field=request.FILES['image'])
        I.save()
        success_url = '../'
        return redirect(success_url)
        pass

    context = {
        'form' : form,
    }
    return render(request,'main/upload_image.html',context)


def upload_dest_image(request):
    
    form  = ImageForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        for image in Image.objects.all():
            if image.im_type == 'dest':
                image.delete()
        I = Image(im_type='dest',im_field=request.FILES['image'])
        I.save()
        success_url = '../'
        return redirect(success_url)
        pass

    context = {
        'form' : form,
    }
    return render(request,'main/upload_image.html',context)

def blend(request):
    FACESWAP_DIR = settings.FACESWAP_DIR
    M = settings.BASE_DIR
    source_image = None
    dest_image = None
    output_image = None
    for image in Image.objects.all():
        if image.im_type == 'source':
            source_image = image
        if image.im_type == 'dest':
            dest_image = image
    

    for image in Image.objects.all():
        if image.im_type == 'output':
            image.delete()
    cmd = "python " + FACESWAP_DIR + "/faceswap.py " + M + source_image.im_field.url + " " + M + dest_image.im_field.url
    print(cmd) 
    if settings.AUTHOR == 'nitish':
        os.system(cmd)
        lcml = "cp "+settings.BASE_DIR+'/output.jpg ' + settings.MEDIA_ROOT
        os.system(lcml)
    local_file = open('media/output.jpg','rb')
    djangofile = File(local_file)
    output_image = Image(im_type='output',im_field=djangofile)
    output_image.save()
    local_file.close()

    success_url = '../'
    return redirect(success_url)
    





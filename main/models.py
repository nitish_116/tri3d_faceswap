from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Image(models.Model):
	im_type = models.CharField(max_length=250)
	im_field = models.FileField()

#!/usr/bin/env python

#  coding: utf-8

###### Searching and Downloading Google Images to the local disk ######

# Import Libraries
import sys
version = (3, 0)
cur_version = sys.version_info
if cur_version >= version:  # If the Current Version of Python is 3.0 or above
    import urllib.request
    from urllib.request import Request, urlopen
    from urllib.request import URLError, HTTPError
    from urllib.parse import quote
    import http.client
    from http.client import IncompleteRead
    http.client._MAXHEADERS = 1000
else:  # If the Current Version of Python is 2.x
    import urllib2
    from urllib2 import Request, urlopen
    from urllib2 import URLError, HTTPError
    from urllib import quote
    import httplib
    from httplib import IncompleteRead
    httplib._MAXHEADERS = 1000
import time  # Importing the time library to check the time of code execution
import os
import argparse
import ssl
import datetime
import json
import re
import codecs
import socket

args_list = ["keywords", "limit", "no_numbering"]

def user_input():
    config = argparse.ArgumentParser()
    config.add_argument('-cf', '--config_file', help='config file name', default='', type=str, required=False)
    config_file_check = config.parse_known_args()
    object_check = vars(config_file_check[0])

    if object_check['config_file'] != '':
        records = []
        json_file = json.load(open(config_file_check[0].config_file))
        for record in range(0,len(json_file['Records'])):
            arguments = {}
            for i in args_list:
                arguments[i] = None
            for key, value in json_file['Records'][record].items():
                arguments[key] = value
            records.append(arguments)
        records_count = len(records)
    else:
        # Taking command line arguments from users
        parser = argparse.ArgumentParser()
        parser.add_argument('-k', '--keywords', help='delimited list input', type=str, required=False)
        parser.add_argument('-l', '--limit', help='delimited list input', type=str, required=False)
        parser.add_argument('-nn', '--no_numbering', default=False, help="Allows you to exclude the default numbering of images", action="store_true")

        args = parser.parse_args()
        arguments = vars(args)
        records = []
        records.append(arguments)
    return records


class googleimagesdownload:
    def __init__(self):
        pass

    # Downloading entire Web Document (Raw Page Content)
    def download_page(self,url):
        version = (3, 0)
        cur_version = sys.version_info
        if cur_version >= version:  # If the Current Version of Python is 3.0 or above
            try:
                headers = {}
                headers['User-Agent'] = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"
                req = urllib.request.Request(url, headers=headers)
                resp = urllib.request.urlopen(req)
                respData = str(resp.read())
                return respData
            except Exception as e:
                print(str(e))
        else:  # If the Current Version of Python is 2.x
            try:
                headers = {}
                headers['User-Agent'] = "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 Safari/537.17"
                req = urllib2.Request(url, headers=headers)
                try:
                    response = urllib2.urlopen(req)
                except URLError:  # Handling SSL certificate failed
                    context = ssl._create_unverified_context()
                    response = urlopen(req, context=context)
                page = response.read()
                return page
            except:
                return "Page Not found"


    # Download Page for more than 100 images
    def download_extended_page(self,url):
        from selenium import webdriver
        from selenium.webdriver.common.keys import Keys
        if sys.version_info[0] < 3:
            reload(sys)
            sys.setdefaultencoding('utf8')
        options = webdriver.ChromeOptions()
        options.add_argument('--no-sandbox')
#        options.add_argument("--headless")

        try:
            browser = webdriver.Chrome('./chromedriver', chrome_options=options)
        except Exception as e:
            print("Looks like we cannot locate the path the 'chromedriver' (use the '--chromedriver' "
                  "argument to specify the path to the executable.) or google chrome browser is not "
                  "installed on your machine (exception: %s)" % e)
            sys.exit()
        browser.set_window_size(1024, 768)

        # Open the link
        browser.get(url)
        time.sleep(1)
        print("Getting you a lot of images. This may take a few moments...")

        element = browser.find_element_by_tag_name("body")
        # Scroll down
        for i in range(5):
            element.send_keys(Keys.PAGE_DOWN)
            time.sleep(0.3)

        for k in range(50):
            try:
                elemento = browser.find_elements_by_xpath("//*[@id='desktopSearchResults']/div[2]/section/div[2]/div")
                print(elemento)
                for i in range(10000):
                    elemento = browser.find_elements_by_xpath("//*[@id='desktopSearchResults']/div[2]/section/div[2]/div")
                    elemento[0].click()
                    element.send_keys(Keys.PAGE_DOWN)
                    time.sleep(10)
            except:
                pass
                #for i in range(5):
                #    element.send_keys(Keys.PAGE_DOWN)
                #    time.sleep(0.3)  # bot id protection

        print("Reached end of Page.")
        time.sleep(0.5)

        source = browser.page_source #page source
        #close the browser
        browser.close()

        return source

    def repair(self,brokenjson):
        invalid_escape = re.compile(r'\\[0-7]{1,3}')  # up to 3 digits for byte values up to FF
        return invalid_escape.sub(self.replace_with_byte, brokenjson)

    # make directories
    def create_directories(self,main_directory, dir_name):
        # make a search keyword  directory
        try:
            if not os.path.exists(main_directory):
                os.makedirs(main_directory)
                time.sleep(0.2)
                path = str(dir_name)
                sub_directory = os.path.join(main_directory, path)
                if not os.path.exists(sub_directory):
                    os.makedirs(sub_directory)
            else:
                path = str(dir_name)
                sub_directory = os.path.join(main_directory, path)
                if not os.path.exists(sub_directory):
                    os.makedirs(sub_directory)
        except OSError as e:
            if e.errno != 17:
                raise
                # time.sleep might help here
            pass
        return

    # Finding 'Next Image' from the given raw page
    def _get_next_item(self,s):
        start_line = s.find('relativePath')
        #print('  get next', start_line)
        if start_line == -1:  # If no links are found then give an error!
            end_quote = 0
            link = "no_links"
            return link, end_quote
        else:
            start_line = s.find('relativePath')
            start_object = s.find(':', start_line + 1)
            end_object = s.find('.jpg', start_object + 1)
            object_raw = str(s[start_object:end_object])
            objj = 'https://myntra.myntassets.com/' + object_raw + '.jpg'
            objj = objj.replace('\\\\','\\')
            objj = objj.replace('\\u002F','/')
            objj = objj.replace(':\\"','')
            #print(objj)
            #with open('links.txt', 'a') as file:
            #    writer = file.write(objj+'\n')

            return objj, end_object


    # Getting all links with the help of '_images_get_next_image'
    def _get_all_items(self,page,main_directory,dir_name,limit,arguments):
        items = []
        errorCount = 0
        i = 0
        count = 1
        while count < limit+1:
            object, end_content = self._get_next_item(page)
            #print('get all', object, end_content)
            if object == "no_links":
                break
            elif object == "":
                page = page[end_content:]
            else:
                os.system('wget -P ./downloads/' + dir_name + ' --no-check-certificate -q ' + object)
                print("Completed Image ====> " + str(count) + ". " + object[object.rfind('/')+1:])
                count += 1
                page = page[end_content:]
            i += 1
        if count < limit:
            print("\n\nUnfortunately " + str(
                limit-count+1) + " could not be downloaded because some images were not downloadable. " + str(
                count-1) + " is all we got for this search filter!")

    # Bulk Download
    def download(self,arguments):

        search_term = 'saree'

        # Setting limit on number of images to be downloaded
        if arguments['limit']:
            limit = int(arguments['limit'])
        else:
            limit = 1000

        main_directory = "downloads"

        paths = {}
        print("Evaluating...")
        dir_name = search_term

        self.create_directories(main_directory,dir_name)     #create directories in OS

        url = 'https://www.myntra.com/' + search_term

        if limit < 1001:
            raw_html = self.download_page(url)  # download page
        else:
            fill = open('inf.txt','r')
            raw_html = fill.read()
            fill.close()
            #raw_html = self.download_extended_page(url)

        #print(raw_html)
        #with open('page_source.html', 'w') as html_file:
        #    writer = html_file.write(raw_html)

        print("Starting Download...")
        self._get_all_items(raw_html,main_directory,dir_name,limit,arguments)    #get all image items and download images


#------------- Main Program -------------#
def main():
    records = user_input()
    #print(records)
    for arguments in records:
        t0 = time.time()  # start the timer
        response = googleimagesdownload()
        response.download(arguments)  #wrapping response in a variable just for consistency

        print("\nEverything downloaded!")
        t1 = time.time()  # stop the timer
        total_time = t1 - t0  # Calculating the total time required to crawl, find and download all the links of 60,000 images
        print("Total time taken: " + str(total_time) + " Seconds")

if __name__ == "__main__":
    main()

import os
#import sys
import numpy as np
from keras.models import model_from_json
from keras.preprocessing.image import img_to_array, load_img
from keras.preprocessing.image import ImageDataGenerator

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

filepath = './test_fonly/'

# Getting model:
model_file = open('./model.json', 'r')
model = model_file.read()
model_file.close()
model = model_from_json(model)

# Getting weights
model.load_weights("./weights.h5")

print('\nPossibilities:')
print("[['blends': 0, 'faces': 1]]\n")

for filename in os.listdir(filepath):
#   img_dir = sys.argv[1]
    if filename.endswith('.jpg') or filename.endswith('.png'):
        img = load_img(filepath + filename, target_size=(150, 150))
        x = img_to_array(img)
        x = x.reshape((1,) + x.shape)
        x/=255

        y_prob = (model.predict(x))[0][0]

        val = 'Blend'
        if y_prob>=0.5:
            val = 'Face'

        print(filename, '\t==>\t', val, '\t--->\t',str(int(y_prob*100)))
